import React,{ Component } from 'react';
import './bootstrap.min.css' //Importamos hoja de estilos
import Header from './components/Header'
import NuevaCita from './components/NuevaCita'
import ListaCitas from './components/ListaCitas'

//cc
class App extends Component {
  state = { 
    citas: []
   }

   //GUARDAR EL ESTADO AL REFRESCAR LA PAGINA INI
// Al cargar la pagina
componentDidMount(){

  
  const citasLS = localStorage.getItem('citas');

  if(citasLS){
    this.setState({
      citas: JSON.parse(citasLS)
    });
  }
}


// Al agregar o eliminar una cita
/* Esto almacena el state de citas en en el localStage pero al recargar 
 * no muestra las citas guardas en la lista de citas, eso se hace en con el 
 *  metodo de componentDidMount()  */
componentDidUpdate(){
  //localStage no admite arrays por lo que pasamos el array de citas a JSON
  localStorage.setItem('citas', JSON.stringify(this.state.citas));
}

   //GUARDAR EL ESTADO AL REFRESCAR LA PAGINA FIN



  crearNuevaCita = datos => {
    console.log(datos);
    
    //Cargamos el state actual e incorporamos la nueva cita
    const citas = [...this.state.citas, datos];

    //Guardamos el state modificado con la nueva cita
    this.setState({
      citas //equivalente a citas : citas al llamarse igual
    });

  }

  //Eliminar Cita
  eliminarCita = id => {
   
    console.log('estamos borrando cita');
    console.log(id);

    // Copiar el state
    const citasActuales = [...this.state.citas];

    // Borrar la cita recorriendo el array de citas

    const citas = citasActuales.filter(cita => cita.id !== id);

    // Actualizar el state

    this.setState({
      citas
    });
  
  }

  render() { 

    return (  
      <div className="container">
        <Header 
            titulo='Administrador Pacientes Veterinaria'/>
    
        <div className="row">
            <div className="col-md-10 mx-auto">
                 <NuevaCita 
                   crearNuevaCita={this.crearNuevaCita} 
                 />
            </div>
        </div>
        <div className="col-md-10 mx-auto"> 
          <ListaCitas 
            citas = {this.state.citas} 
            eliminarCita = {this.eliminarCita}  
          >
          </ListaCitas>
        </div>
      </div>
    );
  }
}
 

export default App;
