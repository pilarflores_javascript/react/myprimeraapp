import React from 'react'; //imr
import PropTypes from 'prop-types'; //para documentar la aplicacion

//sfc
//Lo nombramos como header
const Header = ({titulo}) => (
    <header>
        <h1 className='text-center'>{titulo}</h1>
    </header>
   
)
Header.propTypes = {
    titulo : PropTypes.string.isRequired
}


//Se incluye el nombre del componenete
export default Header;