import React, { Component } from 'react'; //irmc
import uuid from 'uuid';
import PropTypes from 'prop-types';

//Creamos una variable de estado inicial para resetear los valores al enviar el formulario y poner el error como false
const stateInicial = {
    cita : {
        mascota : '',
        propietario : '',
        fecha : '',
        hora : '',
        sintomas : ''
    },
    error : false
}

//cc
class NuevaCita extends Component { 
   /* En el state almacenamos los datos del cliente que se está dando de alta */
    state = { 
        /*  Lo comentamos para resetear al enviar el formulario los valores de los datos  
        //Aqui definimos lo que queremos que lea la aplicacion
        cita : {
      
            mascota : '',
            propietario : '',
            fecha : '',
            hora : '',
            sintomas : '' 
        },
        error : false
        */
       //Copiamos el estado inicial (resetear)
       ...stateInicial

    }
    //Cuando el usuario escribe en los inputs
    handleChange = e => {
        //Muestra en consola lo que se esta escribiendo
        console.log(e.target.name + ' : ' + e.target.value);

        this.setState({
            cita : {
                ...this.state.cita, //Con esto hacemos una copia del objeto para no perderlo al sobreescribir un campo
                [e.target.name] : e.target.value

            }
        })

    }

    //Cuando se envia el formulario
    handleSubmit = e => {

        e.preventDefault();
        // Extraer los datos del state
       const{mascota, propietario, fecha, hora, sintomas} = this.state.cita;


        //Validar datos
        if(mascota === '' || propietario === '' || fecha === '' || hora === '' || sintomas === ''){

            this.setState({
                error : true
            });
            //Detener aplicacion
            return;
        } 

        //Generamos nueva cita con id
        const nuevaCita = {...this.state.cita};
        nuevaCita.id = uuid();

        //Agregar la cita al state de la App
        //Pasamos los datos de un componente hijo al padre a traveés del this.props
        this.props.crearNuevaCita(nuevaCita);

        //Resetamos los valores del formulario
        this.setState({
            ...stateInicial
        })
    }
    

    render() { 

        
        //Extraer el valor del error del state
        /* Para poder mostrar mensaje de error necesitamos estraer el valor de la variable del state*/
        const {error} = this.state;

        return ( 
            <div className="card mt-5 py-5">
                <div className="card-body">
                    <h2 className="card-title text-center mb5">
                        LLena el formulario para crear una nueva cita
                    </h2>

                    {/* Muestra mensaje de error cuando el valor de error del state es true*/}
                    {error ? <div className="alert alert-danger text-center">Todos los campos son obligatorios</div>: null}

                    <form onSubmit={this.handleSubmit}>
                        {/* Comienza campo 1 del formulario */}
                        <div className="form-group row">
                            <label className="col-sm-4 col-lg-2 col-form.laber">Nombre Mascota</label>{/* Es el nombe del campo del formulario*/}
                            <div className="col-sm-8 col-lg-10">
                                <input 
                                id="mascota"
                                type ="text"
                                className="form-control"
                                placeholder="Nombre Macota" //Aparece en el recuadro como sugerencia
                                name="mascota"
                                onChange={this.handleChange}
                                value={this.state.cita.mascota}
                                />
                            </div>
                        </div>{/*form-group*/ }
                        {/* Termina campo 1 del formulario */}
                        {/* Comienza campo 2 del formulario */}
                        <div className="form-group row">
                            <label className="col-sm-4 col-lg-2 col-form.laber">Nombre Dueño</label>{/* Es el nombe del campo del formulario*/}
                            <div className="col-sm-8 col-lg-10">
                                <input 
                                id="propietario"
                                type ="text"
                                className="form-control"
                                placeholder="Nombre Dueño" //Aparece en el recuadro como sugerencia
                                name="propietario"
                                onChange={this.handleChange}
                                value={this.state.cita.propietario}
                                />
                            </div> 
                        </div>{/*form-group*/ }
                        {/* Termina campo 2 del formulario */}
                        {/* Comienza campo 3 del formulario */}
                        <div className="form-group row">
                            <label className="col-sm-4 col-lg-2 col-form.laber">Fecha</label>{/* Es el nombe del campo del formulario*/}
                            <div className="col-sm-8 col-lg-4">
                                <input 
                                id="fecha"
                                type ="date"
                                className="form-control"
                                name="fecha"
                                onChange={this.handleChange}
                                value={this.state.cita.fecha}
                                />
                            </div> {/*form-group*/ }
                       {/*  </div>  lo comentamos para que los dos campos apareszca*/ } 
                         {/* Termina campo 3 del formulario */}
                        {/* Comienza campo 4 del formulario */}
                      {/*  <div className="form-group row"> Inserta fila debajo, al eliminarlo ponemos el campo a un lado del anterior} */}
                            <label className="col-sm-4 col-lg-2 col-form.laber">Hora</label>{/* Es el nombe del campo del formulario*/}
                            <div className="col-sm-8 col-lg-4">
                                <input 
                                id="hora"
                                type ="time"
                                className="form-control"
                                name="hora"
                                onChange={this.handleChange}
                                value={this.state.cita.hora}
                                />
                            </div> {/*form-group*/ }
                         </div> 
                         {/* Termina campo 4 del formulario */}
                         {/* Comienza campo 5 del formulario */}
                        <div className="form-group row">
                            <label className="col-sm-4 col-lg-2 col-form.laber">Síntomas</label>{/* Es el nombe del campo del formulario*/}
                            <div className="col-sm-8 col-lg-10">
                               <textarea
                               id="sintomas"
                               className="form-control"
                               name="sintomas"
                               placeholder="Describe los sintomas"
                               onChange={this.handleChange}
                               value={this.state.cita.sintomas}
                               ></textarea>
                            </div> 
                        </div>{/*form-group*/ }
                        <input type="submit" value="Agregar nueva cita" className="py-3 mt-2 btn btn-success btn-block" />{/* codigo bootstrap para estilos nosotros materia*/}
                        {/* </input> con className boostrap no se cierra con etiqueta adicinal*/}
                        {/* Termina campo 5 del formulario */}
                    </form>

                </div>
            </div>
         );
    }
}

NuevaCita.propTypes = {
    crearNuevaCita : PropTypes.func.isRequired
}
 
export default NuevaCita;