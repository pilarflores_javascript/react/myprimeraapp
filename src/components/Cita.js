import React from 'react';

const Cita = ({cita, eliminarCita}) => (
    <div className="media mt-3">
       <h3 className="mt-0">{cita.mascota}</h3>
       <p className="cart-text"><span>Nombre dueño: </span>{cita.propietario}</p>
       <button className="btn btn-danger"
            //onClick={eliminarCita} --> no se le pasa el id, no lo pinta
           // onClick={eliminarCita(cita.id)} --> se ejecuta al crear el boton (al incluir cita)
           onClick={()=>{eliminarCita(cita.id)}}
            >Eliminar cita &times;</button>

    </div>
)
    
 
export default Cita;